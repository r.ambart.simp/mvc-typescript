
import { ListDisplayAppointmentDTO } from "./dto/ListDisplayAppointmentDTO";
import { TakeAppointmentDTO } from "./dto/TakeAppointmentDTO";



export interface IAppointmentBusiness {
    takeAppointment(appointment:TakeAppointmentDTO):number;
    displayAppointments():ListDisplayAppointmentDTO[];
}