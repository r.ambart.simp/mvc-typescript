import { Appointment } from "./Appointment";
import { AppointmentRepository } from "./AppointmentRepository";
import { ListDisplayAppointmentDTO } from "./dto/ListDisplayAppointmentDTO";
import { TakeAppointmentDTO } from "./dto/TakeAppointmentDTO";
import { IAppointmentBusiness } from "./IAppointmentBusiness";


export class AppointmentBusiness implements IAppointmentBusiness{


    constructor(private repo:AppointmentRepository){}

    takeAppointment(appointment: TakeAppointmentDTO): number {
        const requestedDate:Date = new Date(appointment.date);
        if(isNaN(requestedDate.getTime())) {
            throw new Error('Invalid Date format');
        }

        if(new Date() > requestedDate) {
            throw new Error('Appointment should not be in the past');
        }

        const appointments = this.repo.getAppointments();

        for(let item of appointments) {
            if(this.dateRangeOverlaps(requestedDate, item.date)) {
                throw new Error('Appointment should not overlap');
            }
        }
        

        return this.repo.add(new Appointment(appointment.label, requestedDate));
        
    }
    displayAppointments(): ListDisplayAppointmentDTO[] {
        return this.repo.getAppointments().map(item => item.toListDisplay());
    }

    private dateRangeOverlaps(firstDate:Date, secondDate:Date) {
        const appointmentTimeMs = 1800000;
        const endFirst = new Date(firstDate.getTime() + appointmentTimeMs);
        const endSecond = new Date(secondDate.getTime() + appointmentTimeMs);
        if (firstDate < secondDate && secondDate < endFirst) return true; // b starts in a
        if (firstDate < endSecond && endSecond < endFirst) return true; // b ends in a
        if (secondDate < firstDate && endFirst < endSecond) return true; // a in b
        return false;
    }

}