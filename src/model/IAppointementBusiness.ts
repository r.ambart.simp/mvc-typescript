import { TakeAppointementDTO } from "../dto/TakeAppointementDTO";

export interface IAppointementBusiness {
    takeAppointement(appointement:TakeAppointementDTO):number;
    displayAppointement();
}