import { Appointment } from "../model/Appointment";
import { ListDisplayAppointmentDTO } from "../model/dto/ListDisplayAppointmentDTO";


export class AppointmentView {
    private form:HTMLFormElement;
    onFormSubmit:Function;

    constructor(){
        this.init();
    }

    init() {
        this.form =  document.querySelector('#add-appointment');
        this.form.addEventListener('submit', this.submitForm.bind(this));
    }

    submitForm(event:Event) {
        event.preventDefault();

        const data = new FormData(this.form);

        this.onFormSubmit(data.get('date').toString(), data.get('time').toString(), data.get('label').toString());
    }

    renderList(list:ListDisplayAppointmentDTO[]) {
        
        
        
        const ul = document.querySelector('#list-appointments');
        ul.innerHTML = '';
        for(let item of list) {

            const li = document.createElement('li');
            li.textContent = item.label + ' ' + item.date;
            ul.appendChild(li);
        }
    }
}