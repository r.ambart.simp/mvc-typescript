
export interface ListDisplayAppointmentDTO {
    id:number;
    label:string;
    date:string;
    status:string;
}